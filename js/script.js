(function($) {
    $('#home-slider').owlCarousel({
        nav: false,
        singleItem: true,
        loop: true,
        margin: 0,
        autoplay: true,
    });
})(jQuery);